import {Component, OnInit, ViewChild} from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist';
import {Router} from '@angular/router';

import {
    ApexAxisChartSeries,
    ApexChart,
    ChartComponent,
    ApexDataLabels,
    ApexPlotOptions,
    ApexYAxis,
    ApexLegend,
    ApexStroke,
    ApexXAxis,
    ApexFill,
    ApexTooltip,
    ApexGrid,
    ApexNonAxisChartSeries,
    ApexResponsive
} from 'ng-apexcharts';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {DashboardService} from "../services/dashboard.service";

export type ChartOptions = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
    yaxis: ApexYAxis;
    xaxis: ApexXAxis;
    fill: ApexFill;
    tooltip: ApexTooltip;
    stroke: ApexStroke;
    legend: ApexLegend;
    grid: ApexGrid;
};

export type VisitorChartOptions = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    labels: any;
    tooltip: ApexTooltip;
    legend: ApexLegend;
    colors: string[];
    stroke: any;
    dataLabels: ApexDataLabels;
    plotOptions: ApexPlotOptions;
};

export type newsletterchartOptions = {
    series: ApexAxisChartSeries;
    chart: ApexChart;
    xaxis: ApexXAxis;
    stroke: any;
    tooltip: ApexTooltip;
    dataLabels: ApexDataLabels;
    legend: ApexLegend;
    colors: string[];
    markers: any;
    grid: ApexGrid;
};

declare var require: any;

const data: any = require('./data.json');

export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard1.component.html',
    styleUrls: ['./dashboard1.component.scss']
})
export class Dashboard1Component implements OnInit{
    // Barchart
    @ViewChild('chart') chart: ChartComponent;
    public chartOptions: Partial<ChartOptions>;

    @ViewChild('visitor-chart') chart2: ChartComponent;
    public VisitorChartOptions: Partial<VisitorChartOptions>;

    @ViewChild('newsletter-chart') chart3: ChartComponent;
    public newsletterchartOptions: Partial<newsletterchartOptions>;
    allResidentUsers = [];
    total_services:any;
    lastWeekServices:any;
    todaysServices;
    allConciergeUsers = [];
    displayedColumns = ['name', 'email', 'designation', 'action'];
    dataSource;
    newResidents:any;
    outStandingServices:any;
    totalResident:any;
    totalProperties:any;
    newResidentsArray:any;
    todaysServicesListing:any;
    outStandingServicesArray:any;
    constructor(private dbService: DashboardService, private router: Router) {
        this.getAllProperties();
        this.chartOptions = {
            series: [
                {
                    name: 'Pixel',
                    data: [44, 55, 57, 56, 61, 58]
                },
                {
                    name: 'Ample',
                    data: [76, 85, 101, 98, 87, 105]
                }
            ],
            chart: {
                type: 'bar',
                height: 340
            },
            grid: {
                borderColor: 'rgba(0,0,0,.2)',
                strokeDashArray: 3,
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '30%',
                    endingShape: 'flat'
                },

            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: [
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul'
                ]
            },

            legend: {
                show: false,
            },
            fill: {
                colors: ['#26c6da', '#1e88e5'],
                opacity: 1
            },
            tooltip: {
                fillSeriesColor: false,
                marker: {
          show: false,
      },
            }
        };
        this.VisitorChartOptions = {
            series: [45, 15, 27, 18],
            chart: {
                type: 'donut',
                height: 290
            },
            plotOptions: {
                pie: {
                    donut: {
                        size: '80px'
                    }
                }
            },
            tooltip: {
                fillSeriesColor: false,
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                width: 0
            },
            legend: {
                show: false,
            },
            labels: ['Mobile', 'Tablet', 'Desktop', 'Other'],
            colors: ['#1e88e5', '#26c6da', '#745af2', '#eceff1'],
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        }
                    }
                }
            ]
        };
        this.newsletterchartOptions = {
            series: [
                {
                    name: 'Clicked',
                    data: [0, 5000, 15000, 8000, 15000, 9000, 30000, 0]
                },
                {
                    name: 'Sent',
                    data: [0, 3000, 5000, 2000, 8000, 1000, 5000, 0]
                }
            ],
            chart: {
                height: 290,
                type: 'area'
            },
            dataLabels: {
                enabled: false
            },
            markers: {
                size: 3,
            },
            stroke: {
                curve: 'smooth',
                width: '2'
            },
            colors: ['#26c6da', '#1e88e5'],
            legend: {
                show: false,
            },
            grid: {
                borderColor: 'rgba(0,0,0,.2)',
                strokeDashArray: 3,
                yaxis: {
                    lines: {
                        show: true
                    }
                },
                xaxis: {
                    lines: {
                        show: true
                    }
                },
            },
            xaxis: {
                type: 'category',
                categories: [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8'
                ]
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                }
            }
        };
    }

    ngOnInit() {
        this.newResidents = 0;
        this.outStandingServices = 0;
        this.totalResident = 0;
        this.totalProperties = 0;
        this.total_services = 0;
        this.todaysServices = 0;
        let endDate = new Date();
        let startDate = new Date(<any>endDate - 1000 * 60 * 60 * 24 * 10);

      this.dbService.getAllResidentUsers().subscribe((response: any) => {
        this.allResidentUsers = response;
        this.totalResident = response.length;
        let filteredArray = this.allResidentUsers.filter((user) => {
          let date = new Date(user.created_at);
          return date >= startDate &&  date <= endDate
        });
        this.newResidents = filteredArray.length;
        this.newResidentsArray = filteredArray;

      });
      let today = new Date();
      let todaystart = new Date(today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' 00:00:00')
      let todayend = new Date(today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' 23:59:59')
        this.dbService.getAllLastWeekServices().subscribe((response: any) => {
          this.total_services = response.totalServices;
          const bakingServices = [];
          let index = 0;
          const servicesObject = response.response;
          for (let i = 0; i < servicesObject.length; i++) {
            const services = servicesObject[i];
            for (let j = 0; j < services.length; j++) {
              bakingServices[index] = services[j];
              index++;
            }
          }
          this.lastWeekServices = bakingServices;
          let todaysServices = bakingServices.filter((service) => {
            let date = new Date(service.created_at);
            return date >= todaystart && date <= todayend
          });
          this.todaysServices = todaysServices.length;
          this.todaysServicesListing = todaysServices;
          let outStandingServices = bakingServices.filter((service) => {
            return service.service_completed == "false";
          });
           this.outStandingServices = outStandingServices.length;
           this.outStandingServicesArray = outStandingServices;
        });
      this.dbService.getAllConciergesusers().subscribe((response: any) => {
          localStorage.setItem('allCaptains', JSON.stringify(response));
        let allConciergeUsers = response.filter((user) => {
          return user.user_type_id == "3";
        });
        this.allConciergeUsers = allConciergeUsers;
        // tslint:disable-next-line:member-ordering
        this.dataSource = new MatTableDataSource<any>(this.allConciergeUsers);
      });
    }

    getAllProperties(){
        this.dbService.getAllLocations().subscribe((response: any) => {
            this.totalProperties = response.length;
          });
    }
    // This is for the line chart
    // Line chart
    lineChart1: Chart = {
        type: 'Line',
        data: data['LineWithArea'],
        options: {
            low: 0,
            height: 360,
            high: 35000,
            showArea: true,
            fullWidth: true
        }
    };

    // Timeline
    mytimelines: any[] = [
        {
            from: 'Nirav joshi',
            time: '(5 minute ago)',
            image: 'assets/images/users/1.jpg',
            attachment: 'assets/images/big/img2.jpg',
            content:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.'
        },
        {
            from: 'Sunil joshi',
            time: '(3 minute ago)',
            image: 'assets/images/users/2.jpg',
            content:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.',
            buttons: 'primary'
        },
        {
            from: 'Vishal Bhatt',
            time: '(1 minute ago)',
            image: 'assets/images/users/3.jpg',
            attachment: 'assets/images/big/img1.jpg',
            content:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.'
        },
        {
            from: 'Dhiren Adesara',
            time: '(1 minute ago)',
            image: 'assets/images/users/4.jpg',
            content:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero.',
            buttons: 'warn'
        }
    ];

    // bar chart
    public barChartData: Array<any> = [
        { data: [1.1, 1.4, 1.1, 0.9, 1.9, 1, 0.3, 1.1, 1.4, 1.1, 0.9, 1.9], label: 'Cost' }
    ];
    public barChartLabels: Array<any> = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12'
    ];
    public barChartOptions: any = {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        },

        scales: {

            xAxes: [{
                display: false,
                 datasets: [{
                barPercentage: 0.3,
                categoryPercentage: 0.7
                }]
            }],
            yAxes: [{
                display: false
            }]
        }

    };
    public barChartColors: Array<any> = [
        {
            backgroundColor: 'rgba(255, 255, 255, 0.5)',
            hoverBackgroundColor: 'rgba(255, 255, 255, 0.5)',
            hoverBorderWidth: 2,
            hoverBorderColor: 'rgba(255, 255, 255, 0.5)'
        }
    ];
    public barChartLegend = false;
    public barChartType = 'bar';


    newResidentsListing(){
        localStorage.setItem('residentListing', JSON.stringify(this.newResidentsArray));
        this.router.navigate(['/residents/listings',]);
    }

    lastWeekTasksListing(){
        localStorage.setItem('tasksListing', JSON.stringify(this.lastWeekServices));
        this.router.navigate(['/tasks/listings',]);
    }

    todayTasksListing(){
        localStorage.setItem('tasksListing', JSON.stringify(this.todaysServicesListing));
        this.router.navigate(['/tasks/listings',]);
    }

    outstandingTasksListing(){
        localStorage.setItem('tasksListing', JSON.stringify(this.outStandingServicesArray));
        this.router.navigate(['/tasks/listings',]);
    }

    propertiesListing(){
        //localStorage.setItem('tasksListing', JSON.stringify(this.outStandingServicesArray));
        this.router.navigate(['/listing/property',]);
    }

    editCaptain(userId) {
        this.router.navigate(['/captains/' + userId + '/edit']);
    }
    captainDetail(userId) {
        this.router.navigate(['/captains/' + userId + '/view']);
    }

  // tslint:disable-next-line:member-ordering
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

}
