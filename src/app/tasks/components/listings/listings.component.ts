import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import { TasksService } from '../../services/tasks.service';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.css']
})
export class ListingsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'];
  allResidentUsers = [];
  allbuilding = [];
  selectedProperty = '';
  dataSource;
  propertyDropdown =  false;
  captains:any;
  alertMessage:any;
constructor(private router: Router , private service: TasksService ,breakpointObserver: BreakpointObserver) {
  breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
    this.displayedColumns = result.matches ?
      [ 'service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'] :
      [ 'service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'];
  });
  //this.getAllResidents();
  this.getAllLocations();
}
  ngOnInit() {}


  getAllResidents() {
    this.service.getAllResidents().subscribe((response: any) => {
        this.allResidentUsers = response;
        this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });

  }

  getAllLocations() {
    this.service.getAllLocations().subscribe((response: any) => {
      this.allbuilding = response;
    });
  }

  onChange() {
    this.propertyDropdown = true;
    this.alertMessage = "";
    let data = {"property_code":this.selectedProperty};
    this.service.getAllServices(data).subscribe((response: any) => {
    this.getCaptainsByPropertyCode(this.selectedProperty);
    const bakingServices = [];
    let index = 0;
    const servicesObject = response;
      for (let i = 0; i < servicesObject.length; i++) {
        const services = servicesObject[i];
        for (let j = 0; j < services.length; j++) {
          bakingServices[index] = services[j];
          index++;
        }
      }
      this.dataSource = new MatTableDataSource<any>(bakingServices);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.propertyDropdown = false; 

    },err => {
      const bakingServices = [];
      this.dataSource = new MatTableDataSource<any>(bakingServices);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.alertMessage = err.error.message;
      this.propertyDropdown = false; 
    },);
  }

  /**
   * Get location based upon captains code through request.
   * @param propertyCode property code .
   */
  getCaptainsByPropertyCode(propertyCode){
      this.captains = "";
      var data = {
          "user_type":"captain",    // should be captain or resident
          "property_code": propertyCode
      }
      this.service.getAllCaptains(data).subscribe((response: any) => {
          this.captains = response;
        });
  }

  btnClick(btntype) {
      this.router.navigate(['/tasks/' + btntype + '/edit']);
  }

  detail(uerId) {
    this.router.navigate(['/tasks/' + uerId + '/view']);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
