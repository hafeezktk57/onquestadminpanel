import { Routes } from '@angular/router';
import { ListingComponent } from './components/listing/listing.component';
import { EditComponent } from './components/edit/edit.component';
import { ViewComponent } from './components/view/view.component';

export const PropertyListingRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ListingComponent,
        data: {
          title: 'Properties',
          urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Properties'}
          ]
        }
      },
      {
        path: ':id/edit',
        component: EditComponent,
        data: {
          title: 'Edit Property',
          urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Properties', url: '/listing/property'},
            {title: 'Edit Property'}
          ]
        }
      },
      {
        path: ':id/view',
        component: ViewComponent,
        data: {
          title: 'View Property',
          urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Properties', url: '/listing/property'},
            {title: 'View Property'}
          ]
        }
      }
    ]
  }
];
