import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import { PropertyService } from "../../services/property.service";
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    displayedColumns = ['property_name', 'property_code', 'city','management_company','address','Action'];
    allbuilding = [];
    selectedProperty = '';
    dataSource;

  constructor(private router: Router , private service: PropertyService ,breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
      [ 'property_name', 'property_code','address', 'city','management_company','Action'] :
      [ 'property_name', 'property_code','address','city','management_company','Action'];
    });
    this.getAllLocations();
  }
    ngOnInit() {}

    getAllLocations(){
        this.service.getAllProperties().subscribe((response: any) => {
            this.allbuilding = response;
            this.dataSource = new MatTableDataSource<any>(this.allbuilding);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });
    }

    btnClick(btntype) {
      this.router.navigate(['/listing/property/' + btntype + '/edit']);
    }
    detail(uerId) {
      this.router.navigate(['/listing/property/' + uerId + '/view']);
    }

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

}
