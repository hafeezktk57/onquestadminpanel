import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ResidentRoutes } from './residents.routing';
import { ChartistModule } from 'ng-chartist';
import { ChartsModule } from 'ng2-charts';
import { NgApexchartsModule } from 'ng-apexcharts';
import {ResidentviewComponent} from "./components/view/residentview.component";
import {ResidenteditComponent} from "./components/edit/residentedit.component";
import {ResidentlistingComponent} from "./components/listings/residentlisting.component";
import {FormBuilder, FormsModule} from "@angular/forms";
import {ResidentService} from "./services/resident.service";
import {AuthService} from "./services/auth.service";
import {PropertyService} from "../properties/services/property.service";
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        DemoMaterialModule,
        FlexLayoutModule,
        ChartistModule,
        ChartsModule,
        FormsModule,
        NgApexchartsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ResidentRoutes)
    ],
    declarations: [ResidentviewComponent,ResidenteditComponent,ResidentlistingComponent],
    providers:[FormBuilder , ResidentService, AuthService,PropertyService]
})
export class ResidentsModule { }
