import { Routes } from '@angular/router';

import {ResidenteditComponent} from "./components/edit/residentedit.component";
import {ResidentlistingComponent} from "./components/listings/residentlisting.component";
import {ResidentviewComponent} from "./components/view/residentview.component";
import {BasicTableComponent} from "../tables/basic-table/basic-table.component";

export const ResidentRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ResidentlistingComponent,
        data: {
          title: 'Resident',
          urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Residents'}
          ]
        }
      },
      {
        path: ':id/edit',
        component: ResidenteditComponent,
        data: {
          title: 'Edit Resident',
          urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Residents', url: '/residents'},
            {title: 'Edit Resident'}
          ]
        }
      },
      {
        path: ':id/view',
        component: ResidentviewComponent,
        data: {
          title: 'View Resident',
          urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Residents', url: '/residents'},
            {title: 'View Resident'}
          ]
        }
      }
    ]
  }
];
