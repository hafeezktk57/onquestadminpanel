import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ResidentService} from '../../services/resident.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-location-edit',
    templateUrl: './residentedit.component.html',
    styleUrls: ['./residentedit.component.css']
})

export class ResidenteditComponent implements OnInit {
    options: FormGroup;

    @ViewChild('form') profileForm: NgForm;
    isError = false;
    showLoader = false;
    fileData: File = null;
    previewUrl: any = null;
    uploadedFilePath: string = null;
    user = {
        "email": '',
        "building_id": '',
        "phone": '',
        "first_name": '',
        "last_name": '',
        "created_at": '',
        "street_name": '',
        "city": '',
        "state": '',
        "any_pets": '',
        "profile_picture_url": '',
        "user_profile_service_day": ''
    };
    user_id = null;
    alert = {};

    petProfiles = [];
    houseKeeping = [];
    rxPickup = [];
    groceryShoping = [];
    constructor(fb: FormBuilder , private service: AuthService, private route: ActivatedRoute , private residentService: ResidentService) {
        this.options = fb.group({
            hideRequired: false,
            floatLabel: 'auto'
        });
    }
    // For form validator
    email = new FormControl('', [Validators.required, Validators.email]);

    // Sufix and prefix
    hide = true;

    getErrorMessage() {
        return this.email.hasError('required')
            ? 'You must enter a value'
            : this.email.hasError('email')
                ? 'Not a valid email'
                : '';
    }
    ngOnInit() {
        this.user_id = this.route.snapshot.params.id;
        this.service.getUserProfile(this.user_id).subscribe((response) => {
            this.user = response;
        });
        this.residentService.getAllUserprofile(this.user_id).subscribe((response) => {
            this.petProfiles = response.pet_profile[0];
            this.houseKeeping = response.house_keeping[0];
            this.rxPickup = response.rx_pickup[0];
            this.groceryShoping = response.grocery_shoping[0];
        });
    }
    onSubmit () {
        this.showLoader = true;
        const data = {user_id: this.user_id, ...this.profileForm.value};
        this.service.updateUserProfile(data).subscribe((response: any) => {
            if (response) {
                this.alert = {
                    message:  'Edit successfully',
                    type: 'success'
                };
            } else {
                this.isError = true;
            }
            this.showLoader = false;
        });
    }
}
