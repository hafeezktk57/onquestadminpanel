import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ResidentService} from '../../services/resident.service';

@Component({
    selector: 'app-location-edit',
    templateUrl: './residentview.component.html',
    styleUrls: ['./residentview.component.css']
})

export class ResidentviewComponent implements OnInit {

    @ViewChild('form') profileForm: NgForm;
    isError = false;
    showLoader = false;
    fileData: File = null;
    previewUrl: any = null;
    uploadedFilePath: string = null;
    user_id = null;
    alert = {};
    userService = [];
    constructor(private service: AuthService, private route: ActivatedRoute , private residentService: ResidentService) {}
    ngOnInit() {
        this.user_id = this.route.snapshot.params.id;
        this.residentService.getServiceByUserId(this.user_id).subscribe((response) => {
            let index = 0;
            for (let i = 0; i < response.length; i++) {

                    if ((typeof response[i].service === 'undefined') || response[i].service.length == 0) {
                        delete response[i];
                    } else {
                        this.userService[index] = response[i];
                        index++;
                    }
            }
        });
    }
    parseJson(jsonString) {
        return JSON.parse(jsonString);
    }
/*    onSubmit () {
        this.showLoader = true;
        const data = {user_id: this.user_id, ...this.profileForm.value};
        this.service.updateUserProfile(data).subscribe((response: any) => {
            if (response) {
                this.alert = {
                    message:  'Edit successfully',
                    type: 'success'
                };
            } else {
                this.isError = true;
            }
            this.showLoader = false;
        });
    }*/
}
