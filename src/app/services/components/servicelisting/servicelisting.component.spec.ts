import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicelistingComponent } from './servicelisting.component';

describe('ServicelistingComponent', () => {
  let component: ServicelistingComponent;
  let fixture: ComponentFixture<ServicelistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicelistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicelistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
