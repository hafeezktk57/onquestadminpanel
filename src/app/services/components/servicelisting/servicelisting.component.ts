import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ServiceService} from '../../services/service.service';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-servicelisting',
  templateUrl: './servicelisting.component.html',
  styleUrls: ['./servicelisting.component.css']
})
export class ServicelistingComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    displayedColumns = ['id', 'service_name', 'description', 'service_image','service_type'];
    allResidentUsers = [];
    allbuilding = [];
    selectedProperty = '';
    dataSource;

  constructor(private router: Router , private service: ServiceService ,breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        [ 'service_image', 'service_name', 'service_type', 'description', 'Action'] :
        [ 'service_image', 'service_name', 'service_type', 'description', 'Action'];
    });
    this.getAllServices();
  }
    ngOnInit() {}
    getAllServices() {
      this.service.getAllServices().subscribe((response: any) => {
          this.allResidentUsers = response;
          this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
      });

  }
    onChange() {
        this.service.getAllUsers({
            'user_type': 'resident',
            'property_code': this.selectedProperty
        }).subscribe((response: any) => {
            this.allResidentUsers = response;
            this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });

    }
    btnClick(btntype) {
            this.router.navigate(['/services/' + btntype + '/edit']);
    }
    detail(uerId) {
        this.router.navigate(['/services/' + uerId + '/view']);
    }
    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

}
