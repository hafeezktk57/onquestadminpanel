import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable()
export class ServiceService {
  private urls =  {
    'get_all_users':  'https://onquestapp.herokuapp.com/user/allusers',
    'get_all_services':  'https://onquestapp.herokuapp.com/service/getall',
    'get_all_userProfiles': 'https://onquestapp.herokuapp.com/user/allprofiles',
    'get_services_byUserId': 'https://onquestapp.herokuapp.com/services/get',
  };
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      'Allow': 'GET, POST, OPTIONS, PUT, DELETE'
    })
  };
  constructor(private httpClient: HttpClient) {
  }
  getAllUsers(search) {
    return this.httpClient.post<any>(this.urls.get_all_users, search, this.httpOptions).pipe(
      map(res => res.response),
      catchError(errorRes => {
        return throwError(errorRes);
      }));
  }
  getAllServices() {
    return this.httpClient.get<any>(this.urls.get_all_services, this.httpOptions).pipe(
      map(res => res.response),
      catchError(errorRes => {
        return throwError(errorRes);
      }));
  }
  getAllUserprofile(userId) {
    return this.httpClient.post<any>(this.urls.get_all_userProfiles, {user_id : userId}, this.httpOptions).pipe(
      map(res => res),
      catchError(errorRes => {
        return throwError(errorRes);
      }));
  }
  getServiceByUserId(userId) {
    return this.httpClient.post<any>(this.urls.get_services_byUserId, {user_id : userId}, this.httpOptions).pipe(
      map(res => res.response),
      catchError(errorRes => {
        return throwError(errorRes);
      }));
  }

}
