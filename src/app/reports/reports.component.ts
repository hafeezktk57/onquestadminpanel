import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ReportsService} from './services/reports.service';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
export interface Element {
  report_name: string;
  description: string;
}

const ELEMENT_DATA: Element[] = [
  {
    report_name: 'Residents List',
    description: 'Detailed report of all location residents.',
  },
  {
    report_name: 'Captains List',
    description: 'Detailed report of all location captains.',
  }
];
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['report_name', 'description'];
  allResidentUsers = [];
  allbuilding = [];
  selectedProperty = '';
  dataSource;

constructor(private router: Router , private service: ReportsService ,breakpointObserver: BreakpointObserver) {
  breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
    this.displayedColumns = result.matches ?
      [ 'report_name', 'description', 'Action'] :
      [ 'report_name', 'description', 'Action'];
  });
}
  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
  }

  getAllCaptains() {
    this.service.getAllCaptains().subscribe((response: any) => {
        this.allResidentUsers = response;
        this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });

}
getAllResidents() {
  this.service.getAllResidents().subscribe((response: any) => {
      this.allResidentUsers = response;
      this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  });

}
  btnClick(btntype) {
          this.router.navigate(['/services/' + btntype + '/edit']);
  }
  detail(uerId) {
      this.router.navigate(['/services/' + uerId + '/view']);
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
