import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable()
export class ReportsService {
  private urls =  {
    'get_all_captains':  'https://onquestapp.herokuapp.com/user/getcaptains',
    'get_all_residents':  'https://onquestapp.herokuapp.com/user/getresidents',
    'get_all_userProfiles': 'https://onquestapp.herokuapp.com/user/allprofiles',
    'get_services_byUserId': 'https://onquestapp.herokuapp.com/services/get',
  };
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      'Allow': 'GET, POST, OPTIONS, PUT, DELETE'
    })
  };
  constructor(private httpClient: HttpClient) {
  }
  getAllCaptains() {
    return this.httpClient.get<any>(this.urls.get_all_captains, this.httpOptions).pipe(
      map(res => res.response),
      catchError(errorRes => {
        return throwError(errorRes);
      }));
  }
  getAllResidents() {
    return this.httpClient.get<any>(this.urls.get_all_residents, this.httpOptions).pipe(
      map(res => res.response),
      catchError(errorRes => {
        return throwError(errorRes);
      }));
  }
}
