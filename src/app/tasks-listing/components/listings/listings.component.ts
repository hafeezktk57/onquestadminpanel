import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.css']
})
export class ListingsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'];
  allResidentUsers = [];
  allbuilding = [];
  selectedProperty = '';
  dataSource;
  propertyDropdown =  false;
  captains:any;
  alertMessage:any;
constructor(private router: Router, breakpointObserver: BreakpointObserver) {
  breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
    this.displayedColumns = result.matches ?
      [ 'service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'] :
      [ 'service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'];
  });
}
  ngOnInit() {
    let allTasks = JSON.parse(localStorage.getItem('tasksListing'));
    let allCaptains = JSON.parse(localStorage.getItem('allCaptains'));
    allTasks.forEach(element => {
      let captains = allCaptains.filter((user) => {
        return user.property_code == element.building_id || user.property_code == element.building_code;
      });
      element.captains = captains;
    });
    this.dataSource = new MatTableDataSource<any>(allTasks);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  }

  btnClick(btntype) {
      this.router.navigate(['/tasks/' + btntype + '/edit']);
  }

  detail(uerId) {
    this.router.navigate(['/tasks/' + uerId + '/view']);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
