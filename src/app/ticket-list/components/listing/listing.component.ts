import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import { TicketService } from '../../services/ticket.service';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'];
  allResidentUsers = [];
  allbuilding = [];
  selectedProperty = '';
  dataSource;
  propertyDropdown =  false;
  captains:any;
  alertMessage:any;
  propertyCode:any;
  totaltickets:any;
  openTickets:any;
  closedTickets:any;
constructor(private router: Router , private service: TicketService ,breakpointObserver: BreakpointObserver) {
  var url = window.location.href;
  this.propertyCode = url.split("/").pop();
  breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
    this.displayedColumns = result.matches ?
      [ 'service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'] :
      [ 'service_name', 'resident_name', 'property_name', 'captain','status','created_at','service_day'];
  });
  //this.getAllResidents();
  this.getAllTasks();
}
  ngOnInit() {}


  getAllResidents() {
    this.service.getAllResidents().subscribe((response: any) => {
        this.allResidentUsers = response;
        this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    });
  }

  getAllLocations() {
    this.service.getAllLocations().subscribe((response: any) => {
      this.allbuilding = response;
    });
  }

  getAllTasks() {
    this.totaltickets = 0;
    this.openTickets = 0;
    this.closedTickets = 0;
    this.propertyDropdown = true;
    this.alertMessage = "";
    let data = {"property_code":this.propertyCode};
    this.service.getAllServices(data).subscribe((response: any) => {
    this.getCaptainsByPropertyCode(this.propertyCode);
    const bakingServices = [];
    let index = 0;
    const servicesObject = response;
      for (let i = 0; i < servicesObject.length; i++) {
        const services = servicesObject[i];
        for (let j = 0; j < services.length; j++) {
          bakingServices[index] = services[j];
          index++;
        }
      }
      this.totaltickets = bakingServices.length;
      let outStandingServices = bakingServices.filter((service) => {
        return service.service_completed == "false";
      });
      this.openTickets = outStandingServices.length;
      let completeService = bakingServices.filter((service) => {
        return service.service_completed == "true";
      });
      this.closedTickets = completeService.length;
      this.dataSource = new MatTableDataSource<any>(bakingServices);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.propertyDropdown = false; 

    },err => {
      const bakingServices = [];
      this.dataSource = new MatTableDataSource<any>(bakingServices);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.alertMessage = err.error.message;
      this.propertyDropdown = false; 
    },);
  }

  /**
   * Get location based upon captains code through request.
   * @param propertyCode property code .
   */
  getCaptainsByPropertyCode(propertyCode){
      this.captains = "";
      var data = {
          "user_type":"captain",    // should be captain or resident
          "property_code": propertyCode
      }
      this.service.getAllCaptains(data).subscribe((response: any) => {
          this.captains = response;
        });
  }

  btnClick(btntype) {
      this.router.navigate(['/tasks/' + btntype + '/edit']);
  }

  detail(uerId) {
    this.router.navigate(['/tasks/' + uerId + '/view']);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
