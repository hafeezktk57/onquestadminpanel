import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import { ResidentlistingService } from "../../services/residentlisting.service";
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.css']
})
export class ListingsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    displayedColumns = ['id', 'first_name', 'property', 'unit_Number','service_day','email','phone','Action'];
    allResidentUsers = [];
    allbuilding = [];
    selectedProperty = '';
    dataSource;

  constructor(private router: Router , private service: ResidentlistingService ,breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
      [ 'first_name', 'property', 'unit_Number','service_day','email','phone','Action'] :
      [ 'first_name', 'property', 'unit_Number','service_day','email','phone','Action'];
    });
  }
    ngOnInit() {
      this.dataSource = new MatTableDataSource<any>(JSON.parse(localStorage.getItem('residentListing')));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    btnClick(btntype) {
            this.router.navigate(['/residents/' + btntype + '/edit']);
    }
    detail(uerId) {
        this.router.navigate(['/residents/' + uerId + '/view']);
    }
    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

}
