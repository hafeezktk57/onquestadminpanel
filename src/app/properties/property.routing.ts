import { Routes } from '@angular/router';
import { PropertyComponent } from './property/property.component';
export const DashboardsRoutes: Routes = [
  {
    path: ':id',
    component: PropertyComponent,
    data: {
      title: 'Property',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Property' }
      ]
    }
  },
];
