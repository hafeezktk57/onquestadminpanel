import {
    ChangeDetectorRef,
    Component,
    NgZone,
    OnDestroy,
    ViewChild,
    HostListener,
    Directive,
    AfterViewInit,
} from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MediaMatcher } from '@angular/cdk/layout';


import { MenuItems } from '../../../shared/menu-items/menu-items';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import { Router } from '@angular/router';
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: []
})
export class AppSidebarComponent implements OnDestroy {
    public config: PerfectScrollbarConfigInterface = {};
    mobileQuery: MediaQueryList;
    
    private _mobileQueryListener: () => void;
    status: boolean = true;

    itemSelect: number[] = []
    parentIndex: Number;
    childIndex: Number;
    menuItemsData: any;
    propertyItems:any;
    userData:any;
    private urls =  {
        'get_all_locations': 'https://onquestapp.herokuapp.com/location/getall',
    };
   
    setClickedRow(i, j) {
        this.parentIndex = i;
        this.childIndex = j;
    }
    subclickEvent() {
        this.status = true;
    }
    scrollToTop() {
        document.querySelector('.page-wrapper').scroll({
            top: 0,
            left: 0
        });
    }

    constructor(
        private httpClient: HttpClient,
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
        public menuItems: MenuItems,
        public router: Router
       
    ) {
        this.mobileQuery = media.matchMedia('(min-width: 768px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
        this.sideNavProperties();
        this.menuItemsData = this.menuItems.getMenuitem();
        this.userData = JSON.parse(localStorage.getItem('user'));
    }
    sideNavProperties(){
        this.httpClient.get(this.urls.get_all_locations).subscribe(
            (res) => {
                this.propertyItems = res;
                this.propertyItems.response.forEach(element => {
                    element.type = 'subchild';
                    element.subchildren = [{
                    property_code: element.property_code,
                    state: 'calendar',
                    name: 'Calendar',
                    type: '',
                    icon: '',},
                    {
                    property_code: element.property_code,
                    state: 'chat',
                    name: 'Chat',
                    type: '',
                    icon: '',},
                    {
                    property_code: element.property_code,
                    state: 'taskboard',
                    name: 'Taskboard',
                    type: '',
                    icon: '',},
                    {
                    property_code: element.property_code,
                    state: 'ticketlist',
                    name: 'Ticket List',
                    type: '',
                    icon: '',}]
                    
                });
                this.menuItemsData[1].children = this.propertyItems.response;
              },
            (err: HttpErrorResponse) => {
              if (err.error instanceof Error) {
                console.log('Client-side error occured.');
              } else {
                if ( err.error &&  err.error.text) {
                  return;
                }
                console.log('Server-side error occured.');
              }
            }
          );
    }

    navigate(state, property_code){
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['property',property_code]);
        });
    }
    
    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    logout(){
        localStorage.removeItem("user");
        this.router.navigate(['/login']);
      }
}
