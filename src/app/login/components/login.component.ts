/**
 * Created by Hamza on 5/7/2020.
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import { NgForm } from '@angular/forms';
import {AuthService} from '../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    @ViewChild('form') loginForm: NgForm;
    isError = false;
    showLoader = false;
    constructor(private service: AuthService, private router: Router) { }

    ngOnInit() {
    }
    onSubmit() {
        this.showLoader = true;
        this.isError = false;
        this.service.login(this.loginForm.value).subscribe((response) => {
            if (response) {
                localStorage.setItem('user', JSON.stringify(response));
                this.router.navigate(['/']);
            } else {
                this.isError = true;
            }
            this.showLoader = false;
        }, (err) => {
            this.showLoader = false;
            this.isError = true;
        });
    }

}
