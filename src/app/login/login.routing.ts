import { Routes } from '@angular/router';

import {BasicTableComponent} from "../tables/basic-table/basic-table.component";
import {LoginComponent} from "./components/login.component";

export const LoginRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: LoginComponent,
        data: {
          title: 'Login',
  /*        urls: [
            {title: 'Home', url: '/dashboard'},
            {title: 'Resident'}
          ]*/
        }
      }
    ]
  }
];
